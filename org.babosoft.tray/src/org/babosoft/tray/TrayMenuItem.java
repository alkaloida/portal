/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.tray;

import java.awt.Image;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JMenuItem;

/**
 *
 * @author szlelesz
 */
public class TrayMenuItem extends JMenuItem implements Comparable<TrayMenuItem>{
    private final Integer count;
    private boolean withSeparator = false;
    
    
    public TrayMenuItem(Integer count,String label,ActionListener listener){
        this(count, label, null, listener);
    }
    
    public TrayMenuItem(Integer count,String label){
        this(count, label, null, null);
    }
    
    public TrayMenuItem(Integer count,String label,Image image,ActionListener listener){
        super(label);
        this.count = count;
        if (image != null){
            this.setIcon(new ImageIcon(image));
        }
        if (listener != null){
            this.addActionListener(listener);    
        }        
    }
    
    
    @Override
    public int compareTo(TrayMenuItem o) {        
        return this.getCount().compareTo(o.getCount());
    }

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @return the withSeparator
     */
    public boolean isWithSeparator() {
        return withSeparator;
    }

    /**
     * @param withSeparator the withSeparator to set
     * @return 
     */
    public TrayMenuItem setWithSeparator(boolean withSeparator) {
        this.withSeparator = withSeparator;
        return this;
    }
    
    public TrayMenuItem disabled(){
        super.setEnabled(false);
        return this;
    }
    
    public TrayMenuItem enabled(){
        super.setEnabled(true);
        return this;
    }
}

