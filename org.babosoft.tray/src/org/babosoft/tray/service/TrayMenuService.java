/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.tray.service;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JPopupMenu;
import org.babosoft.tray.TrayMenu;
import org.babosoft.tray.TrayMenuItem;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = TrayMenu.class)
public class TrayMenuService implements TrayMenu{

    private final SystemTray tray = SystemTray.getSystemTray();
    private final ImageIcon image;
    private final TrayIcon trayIcon;
    private JPopupMenu popupMenu = new JPopupMenu();
    private final List<TrayMenuItem> menuItems = new ArrayList<>();
    
    public boolean isSupported() {
        return SystemTray.isSupported();
    }

    @Override
    public void addMenu(TrayMenuItem menuItem) {
        menuItems.add(menuItem);
        Collections.sort(menuItems);        
        this.popupMenu.removeAll();
        for(TrayMenuItem trayMenuItem : this.menuItems){
            this.popupMenu.add(trayMenuItem);
            if (trayMenuItem.isWithSeparator()){
                this.popupMenu.addSeparator();
            }
        }        
    }

    public TrayMenuService() throws AWTException {
        this.image = new ImageIcon(getClass().getResource("images/gubo.png"));
        this.trayIcon = new TrayIcon(image.getImage());
        trayIcon.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON3) {                    
                    popupMenu.setLocation(e.getX(), e.getY() - 15);
                    popupMenu.setInvoker(popupMenu);
                    popupMenu.setVisible(true);
                }
            }
        });
        tray.add(trayIcon);
    }

    @Override
    public Image getImage(Class<?> owner, String url) {
        return new ImageIcon(owner.getResource(url)).getImage();        
    }

    
    
}
