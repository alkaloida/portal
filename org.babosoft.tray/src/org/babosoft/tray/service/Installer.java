/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.tray.service;

import java.awt.event.ActionEvent;
import org.babosoft.tray.TrayMenu;
import org.babosoft.tray.TrayMenuItem;
import org.openide.LifecycleManager;
import org.openide.modules.ModuleInstall;
import org.openide.util.Lookup;

public class Installer extends ModuleInstall {    
    public TrayMenu getTrayMenu(){
        return Lookup.getDefault().lookup(TrayMenu.class);
    }        
    
    @Override
    public void restored() {                
        getTrayMenu().addMenu(new TrayMenuItem(Integer.MAX_VALUE, "Exit",getTrayMenu().getImage(Installer.class, "images/exit.png"), (ActionEvent e) -> {
            LifecycleManager.getDefault().exit();
        }));
    }

}
