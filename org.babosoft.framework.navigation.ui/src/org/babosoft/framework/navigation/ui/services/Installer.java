/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.framework.navigation.ui.services;

import org.babosoft.zk.Framework;
import org.openide.modules.ModuleInstall;
import org.openide.util.Lookup;

public class Installer extends ModuleInstall {
    public Framework getFramework(){
        return Lookup.getDefault().lookup(Framework.class);
    }
    
    
    @Override
    public void restored() {
        getFramework().install(Installer.class, "web");        
    }

}
