/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.framework.navigation.ui.services;

import org.babosoft.framework.navigation.ui.Menu;

import org.babosoft.portal.framework.navigation.MenuItem;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = Menu.class)
public class MenuService implements Menu{
    @Override
    public void addMenu(MenuItem menuItem) {
        org.babosoft.portal.framework.navigation.Menu.get().add(menuItem);        
    }

    @Override
    public Iterable<MenuItem> getMenu() {
        return org.babosoft.portal.framework.navigation.Menu.get().getItems();
    }
    
    
}
