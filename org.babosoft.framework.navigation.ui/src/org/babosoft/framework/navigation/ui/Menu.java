/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.framework.navigation.ui;

import org.babosoft.portal.framework.navigation.MenuItem;

/**
 *
 * @author szlelesz
 */
public interface Menu {
    public void addMenu(MenuItem menuItem);
    public Iterable<MenuItem> getMenu();
}
