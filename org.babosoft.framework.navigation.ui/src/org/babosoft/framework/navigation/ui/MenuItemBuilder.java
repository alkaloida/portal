/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.framework.navigation.ui;

import java.util.ArrayList;
import java.util.List;
import org.babosoft.portal.framework.navigation.MenuItem;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */

@ServiceProvider(service = MenuItemBuilder.class)
public class MenuItemBuilder{
    public MenuItem build(String id, Integer count, String title, String iconClass,String includeUri){
        return build(id, count, title, iconClass, null, includeUri, null);
    }
    
    public MenuItem build(String id, Integer count, String title, String iconClass){
        return build(id, count, title, iconClass, null);
    }
    
    public MenuItem build(String id, Integer count, String title, String iconClass,String toolTip,String includeUri,String badgeText){
        return new MenuItem() {
            List<MenuItem> items = new ArrayList<>();
            @Override
            public String getTitle() {
                return title;
            }

            @Override
            public String getIconSClass() {
                return iconClass;
            }

            @Override
            public String getToolTip() {
                return toolTip;
            }

            @Override
            public String getIncludeUri() {
                return includeUri;
            }

            @Override
            public List<MenuItem> getChildren() {
                return items;
            }

            @Override
            public String getBadgeText() {
                return badgeText;
            }

            @Override
            public String getId() {
                return id;
            }

            @Override
            public Integer getCount() {
                return count;
            }
            @Override
            public int compareTo(MenuItem o) {
                return this.getCount().compareTo(o.getCount());
            }
        };
    }
}
