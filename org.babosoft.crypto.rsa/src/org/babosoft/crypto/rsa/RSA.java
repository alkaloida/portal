/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.rsa;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Lelesz Szabolcs <lelesz.szabolcs@gmail.com>
 */
public interface RSA {

    public KeyPair generateKey() throws NoSuchAlgorithmException;
    
    public KeyPair generateKey(RSAKeySize keySize) throws NoSuchAlgorithmException;

    public byte[] encrypt(byte[] text, Key key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException;
    
    public byte[] decrypt(byte[] text, Key key) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException;
    
    public void sign() throws NoSuchAlgorithmException;
}
