/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.rsa.services;

import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.babosoft.crypto.rsa.RSA;
import org.babosoft.crypto.rsa.RSAKeySize;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = RSA.class)
public class RSAService implements RSA{    
    private static final String ALGORITHM = "RSA";

    @Override
    public KeyPair generateKey(RSAKeySize keySize) throws NoSuchAlgorithmException {
        final KeyPairGenerator keyGen = KeyPairGenerator.getInstance(ALGORITHM);
        keyGen.initialize(keySize.getKeySize());
        return keyGen.generateKeyPair();
    }
    
    @Override
    public KeyPair generateKey() throws NoSuchAlgorithmException {
        return generateKey(RSAKeySize.RSA1024);
    }

    @Override
    public byte[] encrypt(byte[] text, Key key) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {            
        final Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(text);        
    }

    @Override
    public byte[] decrypt(byte[] text, Key key) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {                
        final Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key);        
        return cipher.doFinal(text);
        
    }
    
    @Override
    public void sign() throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("MD5");
    }
}
