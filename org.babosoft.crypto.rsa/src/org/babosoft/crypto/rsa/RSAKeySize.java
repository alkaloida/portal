/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.rsa;

/**
 *
 * @author szlelesz
 */
public enum RSAKeySize {
    RSA1024,  
    RSA2048,
    RSA4096;
    
    public Integer getKeySize(){
        switch(this){
            case RSA1024 : return 1024;
            case RSA2048 : return 2048;
            case RSA4096 : return 4096;    
        }
        return 2048;
    }
}
