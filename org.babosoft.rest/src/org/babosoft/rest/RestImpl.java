/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author szlelesz
 */
public class RestImpl {
     
    public <T> List<T> instance(InputStream jsonStream,Class<T> resultType) throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();
        List<T> result = new ArrayList<>();
        for(Object o : objectMapper.readValue(jsonStream, result.getClass())){
            result.add(objectMapper.convertValue(o, resultType));
        }                
        return result;
    }
    
    
    
    public <T> List<T> instance(Reader jsonReader,Class<T> resultType) throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();
        List<T> result = new ArrayList<>();
        for(Object o : objectMapper.readValue(jsonReader, result.getClass())){
            result.add(objectMapper.convertValue(o, resultType));
        }
        return result;
    }
    
    
    public <T> List<T> instance(String json,Class<T> resultType) throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();
        List<T> result = new ArrayList<>();        
        for(Object o : objectMapper.readValue(json, result.getClass())){
            result.add(objectMapper.convertValue(o, resultType));
        }
        return result;
    }
    
    
    public void write(ByteArrayOutputStream stream ,Object object) throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();        
        objectMapper.writeValue(stream, object);        
    }
    
    
    public String write(Object object) throws IOException{
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}
