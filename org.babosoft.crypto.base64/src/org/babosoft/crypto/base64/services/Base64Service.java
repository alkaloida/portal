/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.base64.services;

import java.nio.charset.Charset;
import org.babosoft.crypto.base64.Base64;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = Base64.class)
public class Base64Service implements Base64{
 
    private static final Charset UTF_8 = Charset.forName("UTF-8");
    
    @Override
    public String encrypt(byte[] chiperText,Charset charSet){
        byte[] base64 = new org.apache.commons.codec.binary.Base64().encode(chiperText);
        return new String(base64,charSet);
    }
    
    @Override
    public String decrypt(byte[] chiperText,Charset charSet){
        byte[] text = new org.apache.commons.codec.binary.Base64().decode(chiperText);
        return new String(text,charSet);
    }
    @Override
    public String encrypt(byte[] chiperText){
        return this.encrypt(chiperText, UTF_8);
    }
    
    @Override
    public String decrypt(byte[] chiperText){
        return this.decrypt(chiperText, UTF_8);
    }
    
            
    
}
