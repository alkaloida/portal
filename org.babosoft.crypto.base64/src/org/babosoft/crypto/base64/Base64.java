/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.base64;

import java.nio.charset.Charset;

/**
 *
 * @author szlelesz
 */
public interface Base64 {
    public String encrypt(byte[] chiperText,Charset charSet);    
    public String decrypt(byte[] chiperText,Charset charSet);
    public String encrypt(byte[] chiperText);    
    public String decrypt(byte[] chiperText);
}
