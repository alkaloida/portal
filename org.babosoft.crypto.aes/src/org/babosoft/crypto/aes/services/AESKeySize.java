/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.aes.services;

/**
 *
 * @author szlelesz
 */
public enum AESKeySize {
    AES128,
    AES196,
    AES256;
    
    public Integer getKeySize(){
        switch(this){
            case AES128 : return 128;
            case AES196 : return 196;
            case AES256 : return 256;
        }
        return 128;
    }
}
