/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.aes.services;

import java.security.AlgorithmParameters;
import java.security.SecureRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.babosoft.crypto.aes.AES;

/**
 *
 * @author szlelesz
 */
public class AESService implements AES {

    private static final String ALGORITHM = "AES";
    private static String salt;
    private static int pswdIterations = 65536;
    private byte[] ivBytes;

    @Override
    public byte[] encrypt(byte[] plainText, String password) throws Exception {
        return this.encrypt(plainText, password, AESKeySize.AES128);
    }

    @Override
    public byte[] decrypt(byte[] encryptedText, String password) throws Exception {
        return this.decrypt(encryptedText, password, AESKeySize.AES128);
    }

    @Override
    public byte[] encrypt(byte[] plainText, String password, AESKeySize keySize) throws Exception {

        //get salt
        salt = generateSalt();
        byte[] saltBytes = salt.getBytes("UTF-8");

        // Derive the key
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        PBEKeySpec spec = new PBEKeySpec(
                password.toCharArray(),
                saltBytes,
                pswdIterations,
                keySize.getKeySize()
        );

        SecretKey secretKey = factory.generateSecret(spec);
        SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), ALGORITHM);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secret);
        AlgorithmParameters params = cipher.getParameters();
        ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
        return cipher.doFinal(plainText);
    }

    //@SuppressWarnings("static-access")
    @Override
    public byte[] decrypt(byte[] encryptedText, String password, AESKeySize keySize) throws Exception {

        byte[] saltBytes = salt.getBytes("UTF-8");        
        // Derive the key
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        PBEKeySpec spec = new PBEKeySpec(
                password.toCharArray(),
                saltBytes,
                pswdIterations,
                keySize.getKeySize()
        );

        SecretKey secretKey = factory.generateSecret(spec);
        SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), ALGORITHM);

        // Decrypt the message
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(ivBytes));
        try {
            return cipher.doFinal(encryptedText);
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new RuntimeException(e);
        }

    }

    private String generateSalt() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        String s = new String(bytes);
        return s;
    }
}
