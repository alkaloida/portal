/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.aes;

import org.babosoft.crypto.aes.services.AESKeySize;

/**
 *
 * @author szlelesz
 */
public interface AES {
    public byte[] encrypt(byte[] plainText,String password) throws Exception;    
    public byte[] decrypt(byte[] encryptedText,String password) throws Exception;
    public byte[] encrypt(byte[] plainText,String password, AESKeySize keySize) throws Exception;     
    public byte[] decrypt(byte[] encryptedText,String password, AESKeySize keySize) throws Exception;
    
}
