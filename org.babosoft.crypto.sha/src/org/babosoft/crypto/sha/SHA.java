/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.sha;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author szlelesz
 */
public interface SHA {
    
    public ByteArrayInputStream getHash(InputStream stream) throws IOException;    
    public String toString(byte[] hash) throws IOException;    
}
