/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.sha.services;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.babosoft.crypto.sha.SHA;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = SHA.class)
public class SHAService implements SHA {

    private static MessageDigest messageDigest = null;

    private MessageDigest GET_MESSAGE_DIGEST() {
        try {
            messageDigest = (messageDigest == null) ? MessageDigest.getInstance("SHA-512") : messageDigest;
            return messageDigest;
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        }
    }

    private byte[] getDigest(InputStream stream) throws IOException {
        byte[] dataBytes = new byte[1024];
        int nread = 0;
        while ((nread = stream.read(dataBytes)) != -1) {
            GET_MESSAGE_DIGEST().update(dataBytes, 0, nread);
        }
        return GET_MESSAGE_DIGEST().digest();
    }

    @Override
    public String toString(byte[] hash){
        StringBuilder sb = new StringBuilder("");
        for (int i = 0; i < hash.length; i++) {
            sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }
    
    @Override
    public ByteArrayInputStream getHash(InputStream stream) throws IOException {        
        return new ByteArrayInputStream(getDigest(stream));
        
    }        
}
