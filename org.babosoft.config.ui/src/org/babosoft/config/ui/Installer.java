/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.config.ui;

import org.babosoft.framework.navigation.ui.Menu;
import org.babosoft.framework.navigation.ui.MenuItemBuilder;
import org.babosoft.portal.framework.navigation.MenuItem;
import org.openide.modules.ModuleInstall;
import org.openide.util.Lookup;

public class Installer extends ModuleInstall {

    public Menu getMenu(){
        return Lookup.getDefault().lookup(Menu.class);
    }
    
    public MenuItemBuilder getMenuItemBuilder(){
        return Lookup.getDefault().lookup(MenuItemBuilder.class);
    }
    
    @Override
    public void restored() {
        MenuItem menu = getMenuItemBuilder().build(null, 100, "Settings", "z-icon-group");
        getMenu().addMenu(menu);
        
    }

}
