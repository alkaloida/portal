/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.browser.services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.babosoft.browser.Browser;
import org.babosoft.tray.TrayMenu;
import org.babosoft.tray.TrayMenuItem;
import org.openide.modules.ModuleInstall;
import org.openide.util.Lookup;

public class Installer extends ModuleInstall {

    public TrayMenu getTrayMenu(){
        return Lookup.getDefault().lookup(TrayMenu.class);
    }
    
    public Browser getBrowser(){
        return Lookup.getDefault().lookup(Browser.class);
    }
    
    
    @Override
    public void restored() {
        getTrayMenu().addMenu(new TrayMenuItem(0, "Open", getTrayMenu().getImage(Installer.class, "images/browser.png"), (ActionEvent e) -> {
            getBrowser().show();
        }).setWithSeparator(true));
    }

}
