/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.browser.services;

import java.io.File;
import java.io.IOException;
import org.babosoft.browser.Browser;
import org.babosoft.environment.Config;
import org.babosoft.environment.Environment;
import org.babosoft.environment.Install;
import org.babosoft.environment.OSType;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = Browser.class)
public final class BrowserService implements Browser {
    private File browserPath = null;
    public File getBrowserFolder(){
        if (browserPath == null){
            browserPath = new File(this.getConfig().getTempFolder()+"/browser");
        }
        return browserPath;
    }
    
    public BrowserService() {
        getEnvironment().delete(getBrowserFolder());        
    }
    
    public Environment getEnvironment(){
        return Lookup.getDefault().lookup(Environment.class);
    }
    
    public Install getInstall(){
        return Lookup.getDefault().lookup(Install.class);
    }
    
    
    public Config getConfig(){
        return Lookup.getDefault().lookup(Config.class);
    }
            

    @Override
    public void show() {
        String[] command = getCommand();
        try {            
            Runtime.getRuntime().exec(command);        
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private String[] getCommand(){
        if (getConfig().getOS().getType() == OSType.Linux){
            return new String[]{"xdg-open","http://localhost:8080"};    
        }
        if (getConfig().getOS().getType() == OSType.Windows){
            return new String[]{"start","http://localhost:8080"};    
        }
        if (getConfig().getOS().getType() == OSType.Mac){
            return new String[]{"open","http://localhost:8080"};    
        }
        throw new RuntimeException("Default browser is unknown ");
    }        
}
