/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.environment.services;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.babosoft.environment.Environment;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = Environment.class)
public class EnvironmentService implements Environment{
    @Override
    public void delete(File path){
        try {
            FileUtils.deleteDirectory(path);
        } catch (IOException ex) {
            Logger.getLogger(EnvironmentService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void create(File path) {
        path.mkdir();
    }
}
