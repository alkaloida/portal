/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.environment.services;

import org.babosoft.environment.Config;
import org.babosoft.environment.OS;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = Config.class)
public class ConfigService implements Config{
    @Override
    public String getTempFolder() {        
        return System.getProperty("java.io.tmpdir");
    }

    @Override
    public OS getOS() {
        return new OS();
    }

    @Override
    public String getUserFolder() {
        return System.getProperty("user.dir");        
    }

    
    
    
}
