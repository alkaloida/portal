/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.environment.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import org.apache.commons.io.FileUtils;
import org.babosoft.environment.Install;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = Install.class)
public class InstallService implements Install{
    
    
    
    private List<JarEntry> getResourceListing(Class<?> clazz, String path) throws URISyntaxException, IOException {
        URL dirURL = clazz.getClassLoader().getResource(path);
        String jarPath = dirURL.getPath().substring(5, dirURL.getPath().indexOf("!"));
        JarFile jar = new JarFile(URLDecoder.decode(jarPath, "UTF-8"));
        //Enumeration<JarEntry> entries = jar.entries(); 
        List<JarEntry> result = new ArrayList<>();
        jar.stream().forEach((jarEntry) -> {
            if (jarEntry.getName().startsWith(path) && !jarEntry.isDirectory()) {
                result.add(jarEntry);
            }
        });
        return result;
    }
    
    private void install(InputStream stream, String path, String name) {
        try {
            FileUtils.copyInputStreamToFile(stream, new File(path + "/" + name));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    } 
              


    @Override
    public void install(Class<?> owner, String resource,String path) {
        try {
            resource += org.apache.commons.lang3.StringUtils.endsWith(resource, "/") ? "" : "/";
            List<JarEntry> jarResources = getResourceListing(owner, resource);
            for (JarEntry entry : jarResources) {
                InputStream inputStream = owner.getClassLoader().getResourceAsStream(entry.getName());
                install(inputStream,path,org.apache.commons.lang3.StringUtils.removeStart(entry.getName(), resource));
            }
        } catch (URISyntaxException | IOException ex) {
            throw new RuntimeException(ex);
        }
    }        
}
