/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.environment;

import java.io.File;

/**
 *
 * @author szlelesz
 */
public interface Environment {
    public void delete(File path);
    public void create(File path);
}
