/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.environment;

import org.apache.commons.lang3.SystemUtils;

/**
 *
 * @author szlelesz
 */
public enum OSType {
    Windows,
    Linux,
    Mac,
    Other;
    
    static OSType get(){
        //os.arch
        if(SystemUtils.IS_OS_LINUX) return Linux;
        if(SystemUtils.IS_OS_WINDOWS) return Windows;
        if(SystemUtils.IS_OS_MAC) return Mac;
        return Other;                
    };
}
