/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.environment;

/**
 *
 * @author szlelesz
 */
public class OS {
   private static Integer arch(){
       String arch = System.getProperty("os.arch").toLowerCase();
       if (arch.equalsIgnoreCase("x86")){
           return 32;
       }
       if (arch.equalsIgnoreCase("amd64")){
           return 64;
       }
       return null;
   }
    
   private final OSType type = OSType.get();
   private final Integer arch = arch();
   public OS(){
       
   }

    /**
     * @return the type
     */
    public OSType getType() {
        return type;
    }

    /**
     * @return the arch
     */
    public Integer getArch() {
        return arch;
    }        
}
