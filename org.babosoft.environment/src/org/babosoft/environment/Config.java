/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.environment;

/**
 *
 * @author szlelesz
 */
public interface Config {
  public String getTempFolder(); 
  public String getUserFolder(); 
  public OS getOS();
}
