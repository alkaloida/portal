/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.zk.services;

import java.io.File;
import org.babosoft.environment.Config;
import org.babosoft.environment.Environment;
import org.babosoft.environment.Install;
import org.babosoft.zk.Framework;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = Framework.class)
public final class FrameworkService implements Framework{
    private File frameworkPath = null;
    public File getFrameworkPath(){
        if (frameworkPath == null){
            frameworkPath = new File(this.getConfig().getTempFolder()+"/portal"); 
        }
        return frameworkPath;
    }
    
    
    public FrameworkService() {
        getEnvironment().delete(getFrameworkPath());                    
        //getEnvironment().create(getFrameworkPath());
    }
    
    public Environment getEnvironment(){
        return Lookup.getDefault().lookup(Environment.class);
    }
    
    public Install getInstall(){
        return Lookup.getDefault().lookup(Install.class);
    }
    
    
    public Config getConfig(){
        return Lookup.getDefault().lookup(Config.class);
    }
                

    @Override
    public void install(Class<?> owner, String resource) {                        
        getInstall().install(owner, resource, getFrameworkPath().getAbsolutePath());  
        
    }          
}
