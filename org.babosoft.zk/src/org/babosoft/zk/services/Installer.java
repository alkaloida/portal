/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.zk.services;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.babosoft.jetty.Server;
import org.babosoft.tray.TrayMenu;
import org.babosoft.tray.TrayMenuItem;
import org.babosoft.zk.Framework;
import org.openide.modules.ModuleInstall;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class Installer extends ModuleInstall {

    private final TrayMenuItem startMenu = new TrayMenuItem(100, "Start", getTrayMenu().getImage(Installer.class, "images/start.png"), new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                getServer().start();
                startMenu.setEnabled(false);
                stopMenu.setEnabled(true);
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    });
    private final TrayMenuItem stopMenu = new TrayMenuItem(101, "Stop", getTrayMenu().getImage(Installer.class, "images/stop.png"), new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                getServer().stop();
                startMenu.setEnabled(true);
                stopMenu.setEnabled(false);
            } catch (Exception ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }).setWithSeparator(true).disabled();

    public Server getServer() {
        return Lookup.getDefault().lookup(Server.class);
    }

    public Framework getFramework() {
        return Lookup.getDefault().lookup(Framework.class);
    }

    public TrayMenu getTrayMenu() {
        return Lookup.getDefault().lookup(TrayMenu.class);
    }

    @Override
    public void restored() {
        getFramework().install(Installer.class, "web");        
        getTrayMenu().addMenu(startMenu);
        getTrayMenu().addMenu(stopMenu);
    }

}
