/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.jetty.services;

import java.io.File;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 *
 * @author szlelesz
 */
public class AppContextBuilder {

    /* private WebAppContext webAppContext;*/

    public WebAppContext buildWebAppContext(String resourceBase) {
        WebAppContext context = new WebAppContext();
        context.setDescriptor("WEB-INF/web.xml");                
        context.setResourceBase(resourceBase);        
        context.setContextPath("/");
        context.setParentLoaderPriority(true);
        return context;
    }
}
