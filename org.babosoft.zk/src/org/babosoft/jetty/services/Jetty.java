/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.jetty.services;

import org.babosoft.environment.Config;
import org.babosoft.jetty.Server;
import org.babosoft.zk.Framework;
import org.babosoft.zk.services.FrameworkService;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = Server.class)
public class Jetty implements Server {    
    private org.eclipse.jetty.server.Server server;
    private Integer port = 8080;

   
    public Framework getFramework(){
        return Lookup.getDefault().lookup(Framework.class);
    }
    
    @Override
    public void start() throws Exception {
        this.getServer().start();
    }

    @Override
    public void stop() throws Exception {
        this.getServer().stop();
    }

    /**
     * @return the port
     */
    @Override
    public Integer getPort() {
        return port;
    }

    /**
     * @param port the port to set
     */
    @Override
    public Server setPort(Integer port) {
        this.port = port;
        return this;
    }

    /**
     * @return the server
     */
    private org.eclipse.jetty.server.Server getServer() {
        if (server == null) {
            ContextHandlerCollection contexts = new ContextHandlerCollection();
            FrameworkService frameworkService = (FrameworkService) getFramework();
            contexts.setHandlers(new Handler[]{new AppContextBuilder().buildWebAppContext(frameworkService.getFrameworkPath().getAbsolutePath())});
            this.server = new org.eclipse.jetty.server.Server(this.getPort());
            this.server.setHandler(contexts);
        }
        return server;
    }


    
    
    

    

    
}
