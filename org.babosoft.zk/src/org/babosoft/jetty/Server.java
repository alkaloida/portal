/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.jetty;

/**
 *
 * @author szlelesz
 */
public interface Server {
    public void start() throws Exception;
    public void stop() throws Exception;
    public Integer getPort();
    public Server setPort(Integer port);
}
