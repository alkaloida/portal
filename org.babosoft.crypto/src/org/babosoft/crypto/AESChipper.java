/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.babosoft.crypto.aes.services.AESKeySize;

/**
 *
 * @author szlelesz
 */
public interface AESChipper {
    public ByteArrayInputStream encrypt(InputStream stream, String password);
    public ByteArrayInputStream decrypt(InputStream stream, String password);
    public ByteArrayInputStream encrypt(InputStream stream, String password, AESKeySize keySize);
    public ByteArrayInputStream decrypt(InputStream stream, String password, AESKeySize keySize);
}
