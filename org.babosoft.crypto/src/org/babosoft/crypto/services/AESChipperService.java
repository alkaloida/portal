/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.services;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.babosoft.crypto.AESChipper;
import org.babosoft.crypto.aes.AES;
import org.babosoft.crypto.aes.services.AESKeySize;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = AESChipper.class)
public class AESChipperService implements AESChipper {

    public AES getAES() {
        return Lookup.getDefault().lookup(AES.class);
    }

    @Override
    public ByteArrayInputStream encrypt(InputStream stream, String password) {
        try {
            return new ByteArrayInputStream(getAES().encrypt(IOUtils.toByteArray(stream), password));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public ByteArrayInputStream decrypt(InputStream stream, String password) {
        try {
            return new ByteArrayInputStream(getAES().decrypt(IOUtils.toByteArray(stream), password));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public ByteArrayInputStream encrypt(InputStream stream, String password, AESKeySize keySize) {
        try {
            return new ByteArrayInputStream(getAES().encrypt(IOUtils.toByteArray(stream), password));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    @Override
    public ByteArrayInputStream decrypt(InputStream stream, String password, AESKeySize keySize){
        try {
            return new ByteArrayInputStream(getAES().decrypt(IOUtils.toByteArray(stream), password));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }
}
