/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.commons.io.IOUtils;
import org.babosoft.crypto.RSAChipper;
import org.babosoft.crypto.rsa.RSA;
import org.babosoft.crypto.sha.SHA;
import org.openide.util.Lookup;

/**
 *
 * @author szlelesz
 */
public class RSAChipperService implements RSAChipper {

    public SHA getSHA() {
        return Lookup.getDefault().lookup(SHA.class);
    }
    
    
    public RSA getRSA() {
        return Lookup.getDefault().lookup(RSA.class);
    }

    @Override
    public ByteArrayInputStream encrypt(InputStream stream, PrivateKey key) {
        try {                    
            return encrypt(IOUtils.toByteArray(stream), key);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public ByteArrayInputStream encrypt(byte[] stream, PrivateKey key) {
        try {
            return new ByteArrayInputStream(getRSA().encrypt(stream, key));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public ByteArrayInputStream decrypt(InputStream stream, PublicKey key) {
        try {
            return decrypt(IOUtils.toByteArray(stream), key);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public ByteArrayInputStream decrypt(byte[] stream, PublicKey key) {
        try {
            return new ByteArrayInputStream(getRSA().decrypt(stream, key));
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException | NoSuchAlgorithmException | NoSuchPaddingException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public ByteArrayInputStream sign(InputStream stream, PrivateKey key){
        try {                        
            return encrypt(getSHA().getHash(stream), key);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public boolean verify(InputStream stream, InputStream sign, PublicKey key){
        try {
            byte[] hash = IOUtils.toByteArray(getSHA().getHash(stream));
            byte[] original = IOUtils.toByteArray(decrypt(sign, key));
            if (hash.length == original.length){
                for(int i = 0; i < hash.length; i++){
                    if(hash[i] != original[i]){
                        return false;
                    }
                }
                return true;
            }
            return false;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
