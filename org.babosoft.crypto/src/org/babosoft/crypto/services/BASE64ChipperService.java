/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto.services;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import org.apache.commons.io.IOUtils;
import org.babosoft.crypto.BASE64Chipper;
import org.babosoft.crypto.base64.Base64;
import org.openide.util.Lookup;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author szlelesz
 */
@ServiceProvider(service = BASE64Chipper.class)
public class BASE64ChipperService implements BASE64Chipper{
    public Base64 getBase64(){
        return Lookup.getDefault().lookup(Base64.class);
    }
    
    @Override
    public String decrypt(InputStream stream){
        return decrypt(stream, null);
    }
    
    @Override
    public String decrypt(InputStream stream,Charset charSet){
        try {
            return (charSet != null) ? 
                    getBase64().decrypt(IOUtils.toByteArray(stream), charSet)
                    :getBase64().decrypt(IOUtils.toByteArray(stream));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    @Override
    public String encrypt(InputStream stream){
        return encrypt(stream, null);
    }
    
    @Override
    public String encrypt(InputStream stream,Charset charSet){
        try {
            return (charSet != null) ? 
                    getBase64().encrypt(IOUtils.toByteArray(stream), charSet)
                    :getBase64().encrypt(IOUtils.toByteArray(stream));
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
    
}
