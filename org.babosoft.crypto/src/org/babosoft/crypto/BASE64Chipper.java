/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto;

import java.io.InputStream;
import java.nio.charset.Charset;

/**
 *
 * @author szlelesz
 */
public interface BASE64Chipper {

    public String decrypt(InputStream stream);

    public String decrypt(InputStream stream, Charset charSet);

    public String encrypt(InputStream stream);

    public String encrypt(InputStream stream, Charset charSet);
}
