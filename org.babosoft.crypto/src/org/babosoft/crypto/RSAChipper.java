/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.babosoft.crypto;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 *
 * @author szlelesz
 */
public interface RSAChipper {
    public ByteArrayInputStream encrypt(InputStream stream,PrivateKey key);
    public ByteArrayInputStream encrypt(byte[] stream, PrivateKey key);
    public ByteArrayInputStream decrypt(InputStream stream, PublicKey key);    
    public ByteArrayInputStream decrypt(byte[] stream, PublicKey key);
    public ByteArrayInputStream sign(InputStream stream, PrivateKey key);
    public boolean verify(InputStream stream, InputStream sign, PublicKey key);
}
